// ==UserScript==
// @name            Hell Storage
// @namespace       hell-storage.user.js
// @license         WTFPL
// @match           https://redacted.ch/requests.php?action=new
// @match           https://orpheus.network/requests.php?action=new
// @grant           GM_getValue
// @grant           GM_setValue
// @grant           GM_listValues
// @grant           GM.getValue
// @grant           GM.setValue
// @grant           GM.listValues
// @version         6.6.6
// @compatible      chrome     Violentmonkey 2.13.0
// @compatible      firefox    Greasemonkey  4.11
// @compatible      firefox    Tampermonkey  4.18.0
// @homepageURL     https://gitlab.com/Iksxsuzil/hellstorage/
// @supportURL      https://gitlab.com/Iksxsuzil/hellstorage/-/issues
// @updateURL       https://gitlab.com/Iksxsuzil/hellstorage/-/raw/master/script.meta.js
// @downloadURL     https://gitlab.com/Iksxsuzil/hellstorage/-/raw/master/script.user.js
// @run-at          document-end
// ==/UserScript==

(function (D) {
  'use strict';

  const TYPES = {
    boolean: (el) => {
      el.dispatchEvent(new MouseEvent('click'));
    },
    string: (el, value) => {
      el.value = value;
    },
  };

  const WHITELIST = [
    'releasetypes_tr',
    'formats_tr',
    'bitrates_tr',
    'media_tr',
    'logcue_tr',
    'voting',
  ];

  const promisify =
    (fn) =>
    (...args) => {
      try {
        return Promise.resolve(fn(...args));
      } catch (error) {
        return Promise.reject(error);
      }
    };

  class Storage {
    constructor() {
      if (GM.info.scriptHandler === 'Greasemonkey') {
        this.get = GM.getValue;
        this.set = GM.setValue;
        this.list = GM.listValues;
      } else {
        this.get = promisify(GM_getValue);
        this.set = promisify(GM_setValue);
        this.list = promisify(GM_listValues);
      }
    }

    read(key) {
      return this.get(key);
    }

    add(key, value) {
      this.set(key, value);
      return true;
    }

    values() {
      return this.list();
    }
  }

  class EventEmitter {
    constructor() {
      this.events = Object.create(null);
    }

    on(name, fn) {
      const event = this.events[name];
      if (event) event.push(fn);
      else this.events[name] = [fn];
    }

    emit(name, ...args) {
      const event = this.events[name];
      if (!event) return;
      for (const fn of event) fn(...args);
    }
  }

  const eventHandler = (event) => {
    const { id, type, value, checked } = event.target;
    const val = type === 'checkbox' ? checked : value;
    ee.emit('add', [id, val]);
  };

  const bindEvent = (element, type, listener, capture) =>
    element.addEventListener(type, listener, capture);

  const main = async (form) => {
    for (const id of await store.values()) {
      const value = await store.read(id);
      const type = typeof value;
      const fn = TYPES[type];
      const el = form.elements.namedItem(id);
      if (el && value) fn(el, value);
    }
    for (const id of WHITELIST) {
      const el = D.getElementById(id);
      if (el) bindEvent(el, 'change', eventHandler, false);
    }
    ee.on('add', async (pars) => await store.add(...pars));
  };

  const store = new Storage();
  const ee = new EventEmitter();

  const form = D.forms['request_form'];
  if (form) main(form);
})(document);
