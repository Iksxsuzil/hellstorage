// ==UserScript==
// @name            Hell Storage
// @namespace       hell-storage.user.js
// @license         WTFPL
// @match           https://redacted.ch/requests.php?action=new
// @match           https://orpheus.network/requests.php?action=new
// @grant           GM_getValue
// @grant           GM_setValue
// @grant           GM_listValues
// @grant           GM.getValue
// @grant           GM.setValue
// @grant           GM.listValues
// @version         6.6.6
// @compatible      chrome     Violentmonkey 2.13.0
// @compatible      firefox    Greasemonkey  4.11
// @compatible      firefox    Tampermonkey  4.18.0
// @homepageURL     https://gitlab.com/Iksxsuzil/hellstorage/
// @supportURL      https://gitlab.com/Iksxsuzil/hellstorage/-/issues
// @updateURL       https://gitlab.com/Iksxsuzil/hellstorage/-/raw/master/script.meta.js
// @downloadURL     https://gitlab.com/Iksxsuzil/hellstorage/-/raw/master/script.user.js
// @run-at          document-end
// ==/UserScript==
